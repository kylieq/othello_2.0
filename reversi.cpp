#include <iostream>
#include <utility>
#include <cmath>
#include <map>
#include <vector>
#include <string>
#include <sstream>

#include "othello.h"

using namespace std;

void PosLi::setPosLi(int size) {
    ctr = 0;
    vector<int> nullV(2,-1);
    for (int i=0; i<size+1; i++) {
        li[i] = nullV;
        ctr += 1;
    }

    x = 0;
    y = 0;
    for (int i=0; i<size; i++) {
        y = 0;
        li[ctr] = nullV;
        ctr += 1;
        int num = 0;
        while (num < size) {
            vector<int> coord(2);
            coord[0] = x;
            coord[1] = y;
            li[ctr] = coord;
            y += 1;
            num += 1;
            ctr += 1;
        }
        x += 1; 
    }    
} 

void NextMove::nextMove(Othello& othello, PosLi pos_li, int player, int& num_occupied) {
    int check1 = requestPos(othello, pos_li, player);
    int check2 = checkIfValid(othello, check1);
      
    while ((check1 == -1) || (check2 == -1)) {
        cout << "Invalid input." << endl;
        if (check1 == -1) {
            cout << "Position not available.\n\n";
        }
        else if (check2 == -1) {
            cout << "Must choose position near a disk that has already been placed.\n\n";
        }
        check1 = requestPos(othello, pos_li, player);
        check2 = checkIfValid(othello, check1);
    }

    othello.occupied[num_occupied+1] = check1;

    if (player == 1) {
        othello.p1_disks += 1;
    }
    else {
        othello.p2_disks += 1;
    }
}

int NextMove::requestPos(Othello othello, PosLi pos_li, int player) {
    int pos;
    string xy_string;

    if (player == 1) {
        cout << "Player 1 turn, input 2 numbers for row and col: ";
    }
    else {
        cout << "Player 2 turn, input 2 numbers for row and col: ";
    }

    int row;
    int col;
    cin >> row;
    cin.ignore();
    cin >> col;

    vector<int> xy(2);
    xy[0] = row;
    xy[1] = col;

    for (int i=othello.size+1; i<pow(othello.size+1,2); i++) {
        if ((pos_li.li[i][0] == xy[0]) && (pos_li.li[i][1] == xy[1])) {
            for (int j=0; j<pow(othello.size+1,2); j++) {
                if (i == othello.occupied[j]) {
                    pos = -1;
                    cout << "pos: " << -1 << endl;
                    return pos;
                }
                else {
                    pos = i;
                }
            }
        }
    }
    return pos;
}

int NextMove::checkIfValid(Othello othello, int pos) {
    for (int i=0; i<pow(othello.size+1,2); i++) {
        if ((pos == othello.occupied[i]+1) || (pos == othello.occupied[i]-1)) {
            return 1;
        }
        else if ((pos == othello.occupied[i] + (othello.size+1)) || (pos = othello.occupied[i] - (othello.size+1))) {
            return 1;
        }
    }
    return -1;
}

void PrintBoard::printBoard(Othello& othello, PosLi pos_li, int c1, int c2, int c3, int c4) {
    cout << "\n  ";

    // Limit only necessary for board size >=10
    if (othello.size >= 10) {
        limit = othello.size-1;
    }
    else {
        limit = othello.size-1;
    }

    // Display column numbers <10.
    for (int i=0; i<=limit; i++) {
       cout << " " << i << " ";
    }

    // Display column numbers >=10 (if applicable).
    for (int i=10; i<othello.size; i++) {
        cout << i << " ";
    }

    cout << "\n";

    int ctr = 0;
    int pos = othello.size+1;

    // Display rows with label <10.
    for (int i=0; i<=limit; i++) {
        cout << ctr << " ";
        pos += 1;
        int j = 0;
        while (j<othello.size) {
            c = determineDisk(othello, pos, c1, c2, c3, c4);
            cout << " " << c << " ";
            pos += 1;
            j += 1;
        }
        ctr += 1;
        cout << "\n";
    }

    // Display rows with label >=10.
    for (int i=10; i<othello.size; i++) {
        cout << ctr;
        pos += 1;
        int j = 0;
        while (j<othello.size) {
            c = determineDisk(othello, pos, c1, c2, c3, c4);
            cout << " " << c << " ";
            pos += 1;
            j += 1;
        }
        ctr += 1;
        cout << "\n";
    }
    cout << "\n";
}

char PrintBoard::determineDisk(Othello& othello, int pos, int c1, int c2, int c3, int c4) {
    for (int k=0; k<pow(othello.size+1,2); k++) {
        diskColor(othello, k, c1, c2, c3, c4);
        if (othello.occupied[k] == pos) {
            if (othello.which_disk == 0) {
                c = othello.p1_disk;
                return c;
            }
            else if (othello.which_disk == 1) {
                c = othello.p2_disk;
                return c;
            }
        }
        else {
            c = othello.empty_slot;
        }
    }
    return c;
}

void PrintBoard::diskColor(Othello& othello, int k, int c1, int c2, int c3, int c4) {
    // If position matches any of the middle positions on board, assign which disk
    // it must hold.
    if ((othello.occupied[k]==c1) || (othello.occupied[k]==c4)) {
        othello.which_disk = 0;
    }

    else if ((othello.occupied[k]==c2) || (othello.occupied[k]==c3)) {
        othello.which_disk = 1;
    }

    // If not center position, then if its index in list occupied is an even number,
    // then it must be player 1's turn. Assign the black (*) disk.
    else if (k%2 == 0) {
        othello.which_disk = 0;
    }

    // If none of the above options, then it must be player 2's turn. Assign the white
    // (o) disk.
    else {
        othello.which_disk = 1;
    }
}
