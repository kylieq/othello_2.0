#include <cmath>
#include <map>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

#ifndef OTHELLO_H_
#define OTHELLO_H_

class Othello {
    public:
        int size;
        int which_disk;
        int p1_disks;
        int p2_disks;
        char p1_disk;
        char p2_disk;
        int p1Pos;
        int p2Pos;
        char empty_slot;
        int occupied[];
};

class PosLi {
    public:
        // (x,y) coordinate of position on board.
        int x;
        int y;
        int ctr;
        map < int, vector<int> > li;

        void setPosLi(int size);
};

class NextMove {
    public:
        int check1;
        int check2;

        void nextMove(Othello& othello, PosLi pos_li, int player, int& num_occupied);
        int requestPos(Othello othello, PosLi pos_li, int player);
        int checkIfValid(Othello othello, int pos);
};

class PrintBoard {
    public:
        int limit;
        char c;

        void printBoard(Othello& othello, PosLi pos_li, int c1, int c2, int c3, int c4);
        char determineDisk(Othello& othello, int pos, int c1, int c2, int c3, int c4);
        void diskColor(Othello& othello, int k, int c1, int c2, int c3, int c4);
};

#endif /* OTHELLO_H_ */
