all: Main.exe
  
Main.exe: Main.o
	g++ -o Main.exe Main.o

Main.o: Main.cpp
	g++ -c Main.cpp

clean:
	rm Main.o Main.exe
