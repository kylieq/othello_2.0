#include <iostream>
#include <utility>
#include <cmath>
#include <map>
#include <vector>
#include <string>
#include <sstream>

#include "othello.h"
#include "reversi.cpp"

using namespace std;

int main() {
    // Create elements in Othello object.
    Othello othello;
    othello.p1_disk = '*';
    othello.p2_disk = 'o';
    othello.empty_slot = '.';
    othello.which_disk = 0;

    PrintBoard board;

    int num_occupied = -1;

    // Initiate size of board.
    int size;
    cout << "Please enter the size of the board: \n";
    cin >> size;
    othello.size = size;

    PosLi pos_li;
    pos_li.setPosLi(othello.size);

    // Board size even
    if (othello.size%2 == 0) {
        int center1 = (pow(othello.size+1, 2))/2;
        int center2 = center1 + 1;
        int center3 = center1 + (othello.size+1);
        int center4 = center3 + 1;

        // Initialize default values of center disks.
        othello.occupied[0] = center1;
        othello.occupied[1] = center2;
        othello.occupied[2] = center3;
        othello.occupied[3] = center4;
        num_occupied += 4;

        int player = -1;

        board.printBoard(othello, pos_li, center1, center2, center3, center4);
        while (num_occupied < pow(othello.size, 2)) {
            if (num_occupied >= pow(othello.size, 2)) {
                break;
            }

            NextMove nm;

            int player = 1;
            nm.nextMove(othello, pos_li, player, num_occupied);
            num_occupied += 1;

            if (num_occupied >= pow(othello.size, 2)) {
                break;
            }

            player = 2;
            nm.nextMove(othello, pos_li, player, num_occupied);
            num_occupied += 1;

            board.printBoard(othello, pos_li, center1, center2, center3, center4);
        }
    }

    // Board size odd
    else {
        int center1 = ((pow(othello.size+1, 2))/2) + (othello.size/2);
        int center2 = center1 + 1;
        int center3 = center1 + (othello.size+1);
        int center4 = center3 + 1;

        // Initialize default values of center disks.
        othello.occupied[0] = center1;
        othello.occupied[1] = center2;
        othello.occupied[2] = center3;
        othello.occupied[3] = center4;
        num_occupied += 4;

        int player = -1;

        board.printBoard(othello, pos_li, center1, center2, center3, center4);
        while (num_occupied < pow(othello.size, 2)) {
            if (num_occupied >= pow(othello.size, 2)) {
                break;
            }

            NextMove nm;

            int player = 1;
            nm.nextMove(othello, pos_li, player, num_occupied);
            num_occupied += 1;

            if (num_occupied >= pow(othello.size, 2)) {
                break;
            }

            player = 2;
            nm.nextMove(othello, pos_li, player, num_occupied);
            num_occupied += 1;

            board.printBoard(othello, pos_li, center1, center2, center3, center4);
        }
    }

    // Determine result of game.
    if (othello.p1_disks == othello.p2_disks) {
        cout << "It's a tie!" << endl;
    }
    else if (othello.p1_disks > othello.p2_disks) {
        cout << "Player 1 won!" << endl;
    }
    else {
        cout << "Player 2 won!" << endl;
    }
}
